
{-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- I don't care enough about this project to have a makefile just run : ghc -o TheThing TheThing.hs



import Data.List
import Data.Tree
import Data.Maybe
import Control.Lens
import Control.Concurrent.Thread.Delay
import Debug.Trace

data Status      = Alive | Dead deriving (Show, Eq)
data World       = World ([([Status], Status, [Status])],
                           ([Status], Status, [Status]),
                          [([Status], Status, [Status])]) deriving (Show)
data Divisions   = DivUpperRight | DivRight  | DivLowerRight |
                   DivUpper      | DivCentre | DivLower      |
                   DivUpperLeft  | DivLeft   | DivLowerLeft
data SquareList  = SquareList [(Integer, Integer)] deriving (Show)
data WorldState  = WorldState World SquareList deriving (Show)

data History     = History {
                        world :: WorldState,

                        prev :: Maybe History,
                        next :: History,
                        alts :: [History]
                    } deriving (Show)



willSurvive   :: Int -> Bool
willResurrect :: Int -> Bool
willSurvive   i = find (\x -> x == i) [2,3] /= Nothing
willResurrect i = find (\x -> x == i) [3]   /= Nothing


naturalWorld :: WorldState
naturalWorld = WorldState cleanWorld $ SquareList []
    where cleanWorld = World ((
            cycle [(cycle [Dead], Dead, cycle [Dead])],
                   (cycle [Dead], Dead, cycle [Dead]),
            cycle [(cycle [Dead], Dead, cycle [Dead])] ))



futureHistory :: WorldState -> History
futureHistory world = thisHistory
    where
        thisHistory = History world Nothing (parentedFutureHistory (getNextIteration world) thisHistory) []

-- the fact that this function works is a testament to how much humanity should fear Haskell compilers
-- it recurses infinitely and copies the parent to itself to point back to itself (see tying the knot) WTF!!!
parentedFutureHistory :: WorldState -> History -> History
parentedFutureHistory world history = thisChildHistory
    where thisChildHistory = History world (Just history) (parentedFutureHistory (getNextIteration world) thisChildHistory) []




iteration :: Integer -> History -> History
iteration i history
    | i == 0    = history
    | i > 0     = iteration (i-1) (next history)
    | i < 0     = iteration (i+1) $fromJust (prev history) -- TODO fromJust returns error, don't want this

branchHistory :: History -> WorldState -> History
branchHistory history world = newHistory
    where
        History oldWorld oldPrev oldNext oldHistory = history
        newHistoryNode = History oldWorld oldPrev oldNext (newHistory : oldHistory)
        newHistory = History world (Just newHistoryNode) (parentedFutureHistory (getNextIteration world) newHistory) []


getNextIteration :: WorldState -> WorldState
getNextIteration x = createNewWorld newAliveList
    where
        createNewWorld :: SquareList -> WorldState
        createNewWorld alives = newWorld
            where
                SquareList as = alives
                newWorld = foldl (\worldState pos -> setElem worldState pos Alive) naturalWorld as

        resurrectedList :: SquareList
        survivedList    :: SquareList
        newAliveList    :: SquareList
        resurrectedList = SquareList $ filter (\pos -> willResurrect (countAlivesAround x pos)) deads
            where SquareList deads = getDeadSquares x
        survivedList    = SquareList $ filter (\pos -> willSurvive   (countAlivesAround x pos)) alives
            where SquareList alives = getAliveSquares x
        newAliveList = SquareList $ resurrected ++ survived
            where
                SquareList resurrected = resurrectedList
                SquareList survived    = survivedList

        getAliveSquares :: WorldState -> SquareList
        getAliveSquares world = alives
            where WorldState _ alives = world
        getDeadSquares :: WorldState -> SquareList
        -- gets the dead squares with an alive adjacent
        getDeadSquares world = SquareList $ foldl (\seen x -> if elem x seen then seen else seen ++ [x]) [] $ -- remove duplicates
                [(x+1, y+1) | (x, y) <- xs, getElem world (x+1, y+1) == Dead] ++
                [(x+1, y)   | (x, y) <- xs, getElem world (x+1, y)   == Dead] ++
                [(x+1, y-1) | (x, y) <- xs, getElem world (x+1, y-1) == Dead] ++
                [(x,   y+1) | (x, y) <- xs, getElem world (x,   y+1) == Dead] ++
                [(x,   y-1) | (x, y) <- xs, getElem world (x,   y-1) == Dead] ++
                [(x-1, y+1) | (x, y) <- xs, getElem world (x-1, y+1) == Dead] ++
                [(x-1, y)   | (x, y) <- xs, getElem world (x-1, y)   == Dead] ++
                [(x-1, y-1) | (x, y) <- xs, getElem world (x-1, y-1) == Dead]
            where WorldState _ (SquareList xs) = world


        countAlivesAround :: WorldState -> (Integer, Integer) -> Int
        countAlivesAround world (x, y) = countThingsAround world (x, y) Alive
        countDeadsAround :: WorldState -> (Integer, Integer) -> Int
        countDeadsAround world (x, y) = countThingsAround world (x, y) Dead
        countThingsAround :: WorldState -> (Integer, Integer) -> Status -> Int
        countThingsAround world (x, y) status=
            (asNum status $ getElem world (x+1, y+1)) +
            (asNum status $ getElem world (x+1, y))   +
            (asNum status $ getElem world (x+1, y-1)) +

            (asNum status $ getElem world (x,   y+1)) +
            (asNum status $ getElem world (x,   y-1)) +

            (asNum status $ getElem world (x-1, y+1)) +
            (asNum status $ getElem world (x-1, y))   +
            (asNum status $ getElem world (x-1, y-1))
            where
                asNum :: Status -> Status -> Int
                asNum expected actual
                    | actual == expected = 1
                    | otherwise          = 0

getWorld :: WorldState -> (Integer, Integer, Integer, Integer) -> [[Status]]
getWorld thisWorldState (x0, y0, x1, y1) = [[ getElem thisWorldState (i, j) | i <- [xi..xf]] | j <- [yi..yf]]
    where (xi, yi, xf, yf) = (min x0 x1, min y0 y1, max x0 x1 + 1, max y0 y1 + 1)

genString :: [[Status]] -> [Char]
genString list = foldl (\str element -> genRow element ++ str) "\n" list
    where
        genRow :: [Status] -> [Char]
        genRow subList = foldl (\str element -> genElem element ++ str) "\n" subList
        genElem :: Status -> [Char]
        genElem status
            | status == Dead  = "."
            | status == Alive = "#"


setElem :: WorldState -> (Integer, Integer) -> Status -> WorldState
setElem thisWorldState position square = WorldState (setWorldElem thisWorld position square) (setAliveListElem thisAliveList position square) -- FIXME: shorter more functional way of doing this??
    where
        WorldState thisWorld thisAliveList = thisWorldState
        setWorldElem :: World -> (Integer, Integer) -> Status -> World
        setWorldElem thisWorld (x, y) square
            | x > 0 = World (setSide right (x, y) square, (upperMiddle, centre, lowerMiddle), left) -- right side
            | x < 0 = World (right, (upperMiddle, centre, lowerMiddle), setSide left (x, y) square) -- left side
                -- middle
                | y > 0 = World (right, (upperMiddle & element (j-1) .~ square, centre, lowerMiddle), left)
                | y < 0 = World (right, (upperMiddle, centre, lowerMiddle & element (j-1) .~ square), left)
                | otherwise = World (right, (upperMiddle, square, lowerMiddle), left)
            where
                World (right, (upperMiddle, centre, lowerMiddle), left) = thisWorld
                (i, j) = (abs (fromIntegral x), abs (fromIntegral y)) -- limiting to Int
                setSide :: [([Status], Status, [Status])] -> (Integer, Integer) -> Status -> [([Status], Status, [Status])]
                setSide side (x, y) square
                    | y > 0 = sideA ++ [(upper & element (j-1) .~ square, centre, lower)] ++ sideB
                    | y < 0 = sideA ++ [(upper, centre, lower & element (j-1) .~ square)] ++ sideB
                    | otherwise = sideA ++ [(upper, square, lower)] ++ sideB
                    where
                        (sideA, other) = splitAt (i-1) side
                        (upper, centre, lower) = head other
                        sideB = tail other
                        (i, j) = (abs (fromIntegral x), abs (fromIntegral y)) -- limiting to Int


        setAliveListElem :: SquareList -> (Integer, Integer) -> Status -> SquareList
        setAliveListElem thisAliveList (x, y) square
            | square == Dead  = SquareList (filter (\(elemX, elemY) -> elemX /= x || elemY /= y) list) --take from list
            | square == Alive = if find (\(elemX, elemY) -> elemX == x && elemY == y) list == Nothing then
                                    SquareList $ (x, y) : list
                                else
                                    thisAliveList
            where SquareList list = thisAliveList



getElem :: WorldState -> (Integer, Integer) -> Status
getElem (WorldState thisWorld thisAliveList) (x, y)
    | x > 0 = getSideElem right (x, y)
    | x < 0 = getSideElem left (x, y)
        | y > 0 = upperMiddle !! (j-1)
        | y < 0 = lowerMiddle !! (j-1)
        | otherwise = centre
    where
        World (right, (upperMiddle, centre, lowerMiddle), left) = thisWorld
        (i, j) = (abs (fromIntegral x), abs (fromIntegral y))
        getSideElem :: [([Status], Status, [Status])] -> (Integer, Integer) -> Status
        getSideElem side (x, y)
            | y > 0 = upper !! (j-1)
            | y < 0 = lower !! (j-1)
            | otherwise = centre
            where
                (sideA, other) = splitAt (i-1) side
                (upper, centre, lower) = head other
                sideB = tail other
                (i, j) = (abs (fromIntegral x), abs (fromIntegral y)) -- limiting to Int


birth :: WorldState -> (Integer, Integer) -> WorldState
kill  :: WorldState -> (Integer, Integer) -> WorldState
birth world pos = setElem world pos Alive
kill  world pos = setElem world pos Dead

myWorldState =  naturalWorld
                `birth` (8, 9)
                `birth` (7, 8)
                `birth` (7, 7)
                `birth` (8, 7)
                `birth` (9, 7)


mainLoop :: WorldState -> IO ()
mainLoop state = do
    putStrLn $ genString $ getWorld state (-10, -10, 10, 10)
    delay(200 * 1000)
    mainLoop $ getNextIteration state

-- main = mainLoop myWorldState

main :: IO ()
main = putStrLn $ genString $ getWorld (world (iteration (-5) (iteration 3 (branchHistory (futureHistory naturalWorld) myWorldState)))) (-10, -10, 10, 10)


